/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   StructureAST.h
 * Author: Jonathan D. Belanger
 *
 * Created on September 22, 2019, 1:02 PM
 */

#ifndef LLVM_BLISS_STRUCTUREAST_H
#define LLVM_BLISS_STRUCTUREAST_H

#include "AST/DeclarationAST.h"

namespace bliss
{

    /*
     * field-reference --- address -+- field-selector
     *                              +- nothing
     *
     * address -+- primary
     *          +- executable-function
     *
     * field-selector --- < position , size -+- , sign-extension-flag -+- >
     *                                       +- nothing ---------------+
     *
     * position -+- expression
     * size -----+
     *
     * sign-extension-flag --- compile-time-constant-expression
     */
    class FieldReferenceAST : public DeclarationAST
    {
        public:
            FieldReferenceAST() {}
            virtual ~FieldReferenceAST();

        private:
            FieldReferenceAST(FieldReferenceAST const&) = delete;               // Prevent copy constructor
            FieldReferenceAST& operator=(FieldReferenceAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * structure-declaration --- STRUCTURE structure-definition, ... ;
     *
     * structure-definition -- structure-name [ -+- access-formal -+--+- : allocation-formal -+- ] =
     *                                           +- nothing -------+  +- nothing -------------+
     *
     *                              -+- [ structure-size ] -+- structure-body
     *                               +- nothing ------------+
     *
     * allocation-formal --- allocation-name -+- = allocation default
     *                                        +- nothing
     *
     * structure-size -+- expression
     * structure-body -+
     *
     * structure-name --+
     * access-formal    +- name
     * allocation-name -+
     *
     * allocation-default -- compile-time-constant-expression
     */
    class StructureDeclarationAST : public DeclarationAST
    {
        public:
            StructureDeclarationAST() {}
            virtual ~StructureDeclarationAST();

        private:
            StructureDeclarationAST(StructureDeclarationAST const&) = delete;               // Prevent copy constructor
            StructureDeclarationAST& operator=(StructureDeclarationAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * field-declaration --- FIELD -+- field-set-definition -+- , ... ;
     *                              +- field-definition -----+
     *
     * field-set-definition --- field-set-name = SET field-definition, ... TES
     *
     * field-definition --- field-name = [ field-component, ... ]
     *
     * field-set-name -+- name
     * field-name -----+
     *
     * field-component --- compile-time-constant-expression
     */
    class FieldDeclarationAST : public DeclarationAST
    {
        public:
            FieldDeclarationAST() {}
            virtual ~FieldDeclarationAST();

        private:
            FieldDeclarationAST(FieldDeclarationAST const&) = delete;               // Prevent copy constructor
            FieldDeclarationAST& operator=(FieldDeclarationAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     *                      +- ordinary-structure-reference
     * structure-reference -+  default-structure-reference
     *                      +- general-structure-reference
     *
     * ordinary-structure-reference --- segment-name [ access-actual, ... ]
     *
     * segment-name --- name
     *
     *                +- field-name
     * access-actual -+  expression
     *                +- nothing
     */
    class OrdinaryStructReferenceAST : public DeclarationAST
    {
        public:
            OrdinaryStructReferenceAST() {}
            virtual ~OrdinaryStructReferenceAST();

        private:
            OrdinaryStructReferenceAST(OrdinaryStructReferenceAST const&) = delete;               // Prevent copy constructor
            OrdinaryStructReferenceAST& operator=(OrdinaryStructReferenceAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * default-structure-reference --- address [ access-actual, ... ]
     */
    class DefaultStructReferenceAST : public DeclarationAST
    {
        public:
            DefaultStructReferenceAST() {}
            virtual ~DefaultStructReferenceAST();

        private:
            DefaultStructReferenceAST(DefaultStructReferenceAST const&) = delete;               // Prevent copy constructor
            DefaultStructReferenceAST& operator=(DefaultStructReferenceAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * general-structure-reference --- structure-name [ access-part -+- : allocation-actual, ... -+- ]
     *                                                               +- nothing ------------------+
     *
     * access-part --- segment-expression -+- , access-actual, ...
     *                                     +- nothing
     *
     * segment-expression -+- expression
     *                     +- nothing
     */
    class GeneralStructReferenceAST : public DeclarationAST
    {
        public:
            GeneralStructReferenceAST() {}
            virtual ~GeneralStructReferenceAST();

        private:
            GeneralStructReferenceAST(GeneralStructReferenceAST const&) = delete;               // Prevent copy constructor
            GeneralStructReferenceAST& operator=(GeneralStructReferenceAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * Predeclared Structures
     *
     * VECTOR       - A vector of signed or unsigned elements of uniform size (BYTE, WORD, LONG, or QUAD)
     * BITVECTOR    - A vector of 1 bit element
     * BLOCK        - A sequence of varying-sized fields
     * BLOCKBYTE    - This structure is functionally equivalent to BLOCK[,BYTE]
     * BLOCKVECTOR  - A vector of BLOCKs
     */
    class BlockDeclarationAST : public DeclarationAST
    {
        public:
            BlockDeclarationAST() {}
            virtual ~BlockDeclarationAST();

        private:
            BlockDeclarationAST(BlockDeclarationAST const&) = delete;               // Prevent copy constructor
            BlockDeclarationAST& operator=(BlockDeclarationAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
            std::map<int, int> fields;
    };
    class VectorDeclarationAST : public DeclarationAST
    {
        public:
            VectorDeclarationAST() : elementSize(0) {}
            virtual ~VectorDeclarationAST();

        private:
            VectorDeclarationAST(VectorDeclarationAST const&) = delete;               // Prevent copy constructor
            VectorDeclarationAST& operator=(VectorDeclarationAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
            std::vector<int> elements;
            uint64_t elementSize;
    };
    class BitVectorDeclarationAST : public DeclarationAST
    {
        public:
            BitVectorDeclarationAST() : numberOfBits(0), size(0) {}
            virtual ~BitVectorDeclarationAST();

        private:
            BitVectorDeclarationAST(BitVectorDeclarationAST const&) = delete;               // Prevent copy constructor
            BitVectorDeclarationAST& operator=(BitVectorDeclarationAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
            std::vector<bool> bits;
            uint64_t numberOfBits;
            uint64_t size;  // in Bytes
    };
}

#endif /* LLVM_BLISS_STRUCTUREAST_H */
