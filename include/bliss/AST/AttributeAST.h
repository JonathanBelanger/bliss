/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   AttributeAST.h
 * Author: Jonathan D. Belanger
 *
 * Created on September 22, 2019, 10:39 AM
 */

#ifndef LLVM_BLISS_ATTRIBUTEAST_H
#define LLVM_BLISS_ATTRIBUTEAST_H

#include "AST/ConstantAST.h"
#include "Lexer/Keyword.h"

namespace bliss
{

    /*
     * 9.15 A Summary of Attribute Usage
     *
     *                      allocation-unit
     *                      |   extension-attribute
     *                      |   |   structure-attribute
     *                      |   |   |   field-attribute
     *                      |   |   |   |   alignment-attribute
     *                      |   |   |   |   |   initial-attribute
     *                      |   |   |   |   |   |   preset-attribute
     *                      |   |   |   |   |   |   |   psect-allocation-attribute
     *                      |   |   |   |   |   |   |   |   volatile-attribute
     *                      |   |   |   |   |   |   |   |   |   no-value-attribute
     *                      |   |   |   |   |   |   |   |   |   |   linkage-attribute
     *                      |   |   |   |   |   |   |   |   |   |   |   range-attribute
     *                      |   |   |   |   |   |   |   |   |   |   |   |   addressing-mode-attribute
     *                      |   |   |   |   |   |   |   |   |   |   |   |   |   weak-attribute
     *                      |   |   |   |   |   |   |   |   |   |   |   |   |   |
     * OWN                  X   X   X   X   X   X   X   X   X   .   .   .   X   .
     * GLOBAL               X   X   X   X   X   X   X   X   X   .   .   .   X   X
     * FORWARD              X   X   X   X   .   .   .   X   X   .   .   .   X   .
     * EXTERNAL             X   X   X   X   .   .   .   X   X   .   .   .   X   X
     *
     * LOCAL                X   X   X   X   X   X   X   .   X   .   .   .   .   .
     * STACKLOCAL           X   X   X   X   X   X   X   .   X   .   .   .   .   .
     * REGISTER             X   X   X   X   .   X   X   .   .   .   .   .   .   .
     * GLOBAL REGISTER      X   X   X   X   .   X   X   .   .   .   .   .   .   .
     * EXTERNAL REGISTER    X   X   X   X   .   X   X   .   .   .   .   .   .   .
     *
     * MAP                  X   X   X   X   .   .   .   .   X   .   .   .   .   .
     *
     * BIND                 X   X   X   X   .   .   .   .   X   .   .   .   .   .
     * GLOBAL BIND          X   X   X   X   .   .   .   .   X   .   .   .   .   X
     *
     * ROUTINE              .   .   .   .   .   .   .   X   .   X   X   .   X   .
     * GLOBAL ROUTINE       .   .   .   .   .   .   .   X   .   X   X   .   X   X
     * FORWARD ROUTINE      .   .   .   .   .   .   .   X   .   X   X   .   X   .
     * EXTERNAL ROUTINE     .   .   .   .   .   .   .   X   .   X   X   .   X   X
     *
     * BIND ROUTINE         .   .   .   .   .   .   .   .   .   X   X   .   .   .
     * GLOBAL BIND ROUTINE  .   .   .   .   .   .   .   .   .   X   X   .   .   X
     *
     * LITERAL              .   .   .   .   .   .   .   .   .   .   .   X   .   .
     * GLOBAL LITERAL       .   .   .   .   .   .   .   .   .   .   .   X   .   X
     * EXTERNAL LITERAL     .   .   .   .   .   .   .   .   .   .   .   X   .   X
     */

    /*
     *            +- allocation-unit
     *            |  extension-attribute
     *            |  structure-attribute
     *            |  field-attribute
     *            |  alignment-attribute
     *            |  initial-attribute
     * attribute -+  preset-attribute
     *            |  psect-allocation
     *            |  volatile-attribute
     *            |  no-value-attribute
     *            |  linkage-attribute
     *            |  range-attribute
     *            |  address-mode-attribute
     *            +- weak-attribute
     *
     * volatile-attribute --- VOLATILE
     *
     * no-value-attribute --- NOVALUE
     *
     * weak-attribute --- WEAK
     */
    class AttributeAST : public BaseAST
    {
        public:
            /* CONSTRUCTOR */
            AttributeAST() : _volatile(false), no_value(false), weak(false) { }

            /* DESTRUCTOR */
            virtual ~AttributeAST() {}

        private:
            AttributeAST(AttributeAST const&) = delete;               // Prevent copy constructor
            AttributeAST& operator=(AttributeAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            bool _volatile;
            bool no_value;
            bool weak;
    };

    /*
     *                  +- BYTE
     * allocation-unit -+  WORD
     *                  |  LONG
     *                  +- QUAD
     */
    class AllocationUnitAST : public AttributeAST
    {
        public:
            typedef enum _attr_alloc_unit
            {
                Quad,
                Long,
                Word,
                Byte,
                Other       // Used to set the default alignment for structures
            } AttrAllocUnit;
            virtual ~AllocationUnitAST() {}

        private:
            AllocationUnitAST(AllocationUnitAST const&) = delete;               // Prevent copy constructor
            AllocationUnitAST& operator=(AllocationUnitAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            uint64_t size;
            AttrAllocUnit type;
    };

    /*
     * extension-attribute -+- SIGNED
     *                      +- UNSIGNED
     */
    class ExtensionAttrAST : public AttributeAST
    {
        public:
            typedef enum _attr_extension
            {
                Unsigned,
                Signed
            } AttrExtension;
            virtual ~ExtensionAttrAST() {}

        private:
            ExtensionAttrAST(ExtensionAttrAST const&) = delete;               // Prevent copy constructor
            ExtensionAttrAST& operator=(ExtensionAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            AttrExtension ext;
    };

    /*
     *                    +- compile-time-constant-expression
     * allocation-actual -+  allocation-unit
     *                    |  extension-attribute
     *                    +- nothing
     */
    class AllocationActualAST : public AttributeAST
    {
        public:
            virtual ~AllocationActualAST() {}

        private:
            AllocationActualAST(AllocationActualAST const&) = delete;               // Prevent copy constructor
            AllocationActualAST& operator=(AllocationActualAST const&) = delete;    // Prevent assignment
    };

    /*
     * structure-attribute -+- REF -----+- structure-name -+--+- [allocation-actual, ... ]
     *                      +- nothing -+                     +- nothing
     *
     * structure-name --- name
     *
     */
    class StructureAttrAST : public AttributeAST
    {
        public:
            /* CONSTRUCTORS */
            StructureAttrAST() : allocActual(nullptr)
            {
                name.clear();
            }

            /* DESTRUCTOR */
            virtual ~StructureAttrAST()
            {
                name.clear();
            }

            /* SETTERS */

            /**
             * This function is called to set the structure name associated
             * with this structure-attribute.
             *
             * @param: name - a string containing the name of the structure.
             */
            void
            setName(std::string Name);

            /**
             * This function is called to set the allocation-actual for
             * this attribute.
             *
             * @param: allocAct - a pointer to AllocationActualAST.
             */
            void
            setAllocActual(AllocationActualAST *allocAct);

            /* GETTERS */

            /**
             * This function is called to return the structure name associated
             * with this structure-attribute
             *
             * @returns: string - the name of the structure-attribute structure
             */
            std::string
            getName()
            {
                return (name);
            }

            /**
             * This function is called to return the allocation-actual
             * information.  The returned value represents the number of bytes.
             *
             * @returns pointer to the AllocationActualAST.
             */
            AllocationActualAST
            *getAllocActual()
            {
                return (allocActual);
            }

        private:
            StructureAttrAST(StructureAttrAST const&) = delete;               // Prevent copy constructor
            StructureAttrAST& operator=(StructureAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            std::string name;
            AllocationActualAST *allocActual;
    };

    /*
     * field-attribute --- FIELD ( -+- field-name -----+- , ... )
     *                              +- field-set-name -+
     *
     * field-name     -+- name
     * field-set-name -+
     */
    class FieldAttrAST : public AttributeAST
    {
        public:
            virtual ~FieldAttrAST();

        private:
            FieldAttrAST(FieldAttrAST const&) = delete;               // Prevent copy constructor
            FieldAttrAST& operator=(FieldAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            std::string name;
    };

    /*
     * alignment-attribute --- ALIGN ( boundary )
     *
     * boundary --- compile-time-constant-expression
     */
    class AlignmentAttrAST : public AttributeAST
    {
        public:
            typedef enum _alignment_attr
            {
                Byte,
                Word,
                Long,
                Quad
            } AlignmentAttr;

            /* DESTRUCTOR */
            virtual ~AlignmentAttrAST();

            /**
             * This function is called to set the boundary alignment.
             *
             * @param boundary - A value representing the boundary to
             *                   be utilized.
             * @return true - if set successfully
             *         false - if an error was detected.
             */
            bool
            set(uint64_t boundary)
            {
                bool retVal = true;

                switch (boundary)
                {
                    case 0:
                        align = Byte;
                        break;

                    case 1:
                        align = Word;
                        break;

                    case 2:
                        align = Long;
                        break;

                    case 4:
                        align = Quad;
                        break;

                    default:
                        align = Quad;
                        retVal = false;
                        break;
                }
                return (retVal);
            }

        private:
            AlignmentAttrAST(AlignmentAttrAST const&) = delete;               // Prevent copy constructor
            AlignmentAttrAST& operator=(AlignmentAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/

            /**
             * This function is called to set the default values for
             * boundary alignment, based on allocation unit type.
             *
             * @param type - An enumeration indicating the Attribute
             *               allocation unit.
             */
            void
            setDefault(AllocationUnitAST::AttrAllocUnit type)
            {
                switch (type)
                {
                    case AllocationUnitAST::Other:
                    case AllocationUnitAST::Quad:
                        align = Quad;
                        break;

                    case AllocationUnitAST::Long:
                        align = Long;
                        break;

                    case AllocationUnitAST::Word:
                        align = Word;
                        break;

                    case AllocationUnitAST::Byte:
                        align = Byte;
                        break;
                }
            }
            AlignmentAttr align;
    };

    /*
     * initial-attribute --- INITIAL ( initial-item, ... )
     *
     *               +- initial-group
     * initial-item -+  initial-expression
     *               +- initial-string
     *
     * initial-group -+- allocation-unit
     *                +- REP replicator OF -+- allocation-unit -+- ( initial-item, ... )
     *                                      +- nothing ---------+
     *
     * replicator --- compile-time-constant-expression
     *
     * initial-expression --- expression
     *
     * initial-string --- string-literal
     */
    class InitialAttrAST : public AttributeAST
    {
        public:
            virtual ~InitialAttrAST();

        private:
            InitialAttrAST(InitialAttrAST const&) = delete;               // Prevent copy constructor
            InitialAttrAST& operator=(InitialAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
    };

    /*
     * preset-attribute --- PRESET ( preset-item, ... )
     *
     * preset-item --- [ ctoe-access-actual, ... ] = preset-value
     *
     * ctoe-access-actual -+- compile-time-constant-expression
     *                     +- field-name
     *
     * preset-value --- expression(1)
     *
     * (1): For OWN and GLOBAL declarations the preset-value must be a
     *      link-time constant expression. For LOCAL, STACKLOCAL, REGISTER,
     *      GLOBAL REGISTER, and EXTERNAL REGISTER declarations the
     *      preset-value can be an executable expression.
     */
    class PresetAttrAST : public AttributeAST
    {
        public:
            virtual ~PresetAttrAST();

        private:
            PresetAttrAST(PresetAttrAST const&) = delete;               // Prevent copy constructor
            PresetAttrAST& operator=(PresetAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
    };

    /*
     * psect-allocation --- PSECT ( psect-name )
     *
     * psect-name --- name
     */
    class PsectAllocationAST : public AttributeAST
    {
        public:
            virtual ~PsectAllocationAST();

        private:
            PsectAllocationAST(PsectAllocationAST const&) = delete;               // Prevent copy constructor
            PsectAllocationAST& operator=(PsectAllocationAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
    };

    /*
     * linkage-attribute --- linkage-name
     *
     * linkage-name --- name
     */
    class LinkageAttrAST : public AttributeAST
    {
        public:
            virtual ~LinkageAttrAST();

        private:
            LinkageAttrAST(LinkageAttrAST const&) = delete;               // Prevent copy constructor
            LinkageAttrAST& operator=(LinkageAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
    };

    /*
     * range-attribute -+- SIGNED ---+- ( bit-count )
     *                  +- UNSIGNED -+
     *
     * bit-count --- compile-time-constant-expression
     */
    class RangeAttrAST : public AttributeAST
    {
        public:
            virtual ~RangeAttrAST();

        private:
            RangeAttrAST(RangeAttrAST const&) = delete;               // Prevent copy constructor
            RangeAttrAST& operator=(RangeAttrAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            ExtensionAttrAST extension;
    };

    /*
     *                            +- GENERAL
     *                            |  ABSOLUTE
     * addressing-mode-attribute -+  LONG_RELATIVE
     *                            |  WORD_RELATIVE
     *                            +- QUAD_RELATIVE
     */
    class AddrModeAST : public AttributeAST
    {
        public:
            typedef enum _addressing_mode
            {
                QuadRelative,
                LongRelative,
                WordRelative,
                Absolute,
                General
            } AddressingMode;
            virtual ~AddrModeAST() {}

        private:
            AddrModeAST(AddrModeAST const&) = delete;               // Prevent copy constructor
            AddrModeAST& operator=(AddrModeAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA*/
            AddressingMode mode;
    };
}

#endif /* LLVM_BLISS_ATTRIBUTEAST_H */
