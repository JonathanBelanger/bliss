/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   IncludeAST.h
 * Author: Jonathan D. Belanger
 *
 * Created on September 28, 2019, 5:10 PM
 */

#ifndef LLVM_BLISS_INCLUDEAST_H
#define LLVM_BLISS_INCLUDEAST_H

#include "AST/BaseAST.h"

namespace bliss
{

    /*
     * require-declaration --- REQUIRE file-designator ;
     *
     * file-designator --- quoted-string
     */
    class RequireAST
    {
        public:
            /* CONSTRUCTOR */
            RequireAST() {}

            /* DESCRUTCOR */
            virtual ~RequireAST();

        private:
            RequireAST(RequireAST const&) = delete;               // Prevent copy constructor
            RequireAST& operator=(RequireAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * library-declaration --- LIBRARY file-designator ;
     */
    class LibraryAST
    {
        public:
            /* CONSTRUCTOR */
            LibraryAST() {}

            /* DESCRUTCOR */
            virtual ~LibraryAST();

        private:
            LibraryAST(LibraryAST const&) = delete;               // Prevent copy constructor
            LibraryAST& operator=(LibraryAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };
}

#endif /* LLVM_BLISS_INCLUDEAST_H */
