/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   SpecialAST.h
 * Author: Jonathan D. Belanger
 *
 * Created on September 28, 2019, 5:46 PM
 */

#ifndef LLVM_BLISS_SPECIALAST_H
#define LLVM_BLISS_SPECIALAST_H

#include "Basic/Lister.h"
#include "Lexer/Keyword.h"
#include "AST/DeclarationAST.h"
#include "AST/StructureAST.h"

namespace bliss
{

    /*
     * psect-declaration --- PSECT psect-item, ... ;
     *
     * psect-item --- storage-class = psect-name -+- ( psect-attribute, ... )
     *                                            +- nothing
     *
     *                +- OWN
     *                |  GLOBAL
     * storage-class -+  PLIT
     *                |  CODE
     *                +- NODEFAULT
     *
     * psect-name --- name
     *
     *                  +- WRITE | NOWRITE
     *                  |  EXECUTE | NOEXECUTE
     *                  |  OVERLAY | NOOVERLAY
     *                  |  READ | NOREAD
     * psect-attribute -+  SHARE | NOSHARE
     *                  |  PIC | NOPIC
     *                  |  LOCAL | GLOBAL
     *                  |  VECTOR
     *                  |  alignment-attribute
     *                  +- addressing-mode-attribute
     */
    class PsectDeclAST : public DeclarationAST
    {
        public:
            PsectDeclAST() {}
            virtual ~PsectDeclAST();

        private:
            PsectDeclAST(PsectDeclAST const&) = delete;               // Prevent copy constructor
            PsectDeclAST& operator=(PsectDeclAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * switches-declaration --- SWITCHES switch-item, ... ;
     *
     * switch-item -+- on-off-switch-item
     *              +- special-switch-item
     *
     *                     +- ERRS | NOERRS
     *                     |  OPTIMIZE | NOOPTIMIZE
     * on-off-switch-item -+  SAFE | NOSAFE
     *                     |  UNAMES | NOUNAMES
     *                     +- ZIP | NOZIP
     *
     *                      +- ADDRESSING_MODE ( mode-spec, ... )
     *                      |  LANGUAGE ( language-list )
     * special-switch-item -+  LINKAGE ( linkage-name )
     *                      |  LIST ( list-option, ... )
     *                      +- STRUCTURE -+- ( structure-attribute )
     *                                    +- nothing
     *
     *                +- COMMON
     * language-list -+  language-name
     *                +- nothing
     *
     * language-name -+- BLISS32
     *                +- BLISS64
     *
     * linkage-name --- name
     *
     *              +- SOURCE | NOSOURCE
     *              |  REQUIRE | NOREQUIRE
     *              |  EXPAND | NOEXPAND
     *              |  TRACE | NOTRACE
     * list-option -+  LIBRARY | NOLIBRARY
     *              |  OBJECT | NOOBJECT
     *              |  ASSEMBLY | NOASSEMBLY
     *              |  SYMBOLIC | NOSYMBOLIC
     *              |  BINARY | NOBINARY
     *              +- COMMENTARY | NOCOMMENTARY
     *
     * mode-spec -+- EXTERNAL ----+-- mode
     *            +- NONEXTERNAL -+
     *
     *       +- GENERAL
     * mode -+  ABSOLUTE
     *       |  LONG_RELATIVE
     *       +- WORD_RELATIVE
     */
    class SwitchesDeclAST : public DeclarationAST
    {
        public:
            typedef enum _lang
            {
                LANG_DEFAULT,
                LANG_COMMON,
                LANG_BLISS32,
                LANG_BLISS64
            } Lang;
            typedef enum _mode_32
            {
                MODE_DEFAULT,
                MODE_GENERAL,
                MODE_ABSOLUTE,
                MODE_LONG_RELATIVE,
                MODE_WORD_RELATIVE
            } Mode32;

            typedef struct _onOffSwitches
            {
                bool code;
                bool debug;
                bool errs;
                bool optimize;
                bool safe;
                bool unames;
                bool zip;
            } OnOffSwitches;

            typedef struct _modeSwitches
            {
                Mode32 external;
                Mode32 nonExternal;
            } ModeSwitches;

            /* CONSTRUCTOR */
            SwitchesDeclAST()
            {
                langListSet = false;
                linkageSet = false;
                onOff.code = true;
                onOff.debug = false;
                onOff.errs = true;
                onOff.optimize = true;
                onOff.safe = true;
                onOff.unames = false;
                onOff.zip = false;
                mode.external = MODE_WORD_RELATIVE;     // TODO: Not always initial value
                mode.nonExternal = MODE_WORD_RELATIVE;  // TODO: Not always initial value
                languageList.push_back(LANG_BLISS32);   // TODO: Not always initial value
                linkage = "BLISS";                      // TODO: Not always initial value
                list.source = true;
                list.require = false;
                list.expand = false;
                list.trace = false;
                list.library = false;
                list.object = true;
                list.assembly = false;
                list.symbolic = true;
                list.binary = true;
                list.commentary = true;
                optLevel = 2;
                return;
            }
            virtual ~SwitchesDeclAST()
            {
                /* TODO: This is just a place-holder to keep the linker happy. */
            }

            /* SETTERS */

            /**
             * Set the on/off switch, based on the supplied parameter.
             *
             * param: keyword (NO* = off, otherwise on).
             */
            void setOnOff(KWD::Keyword keyword);

            /**
             * Set the language-list, based on the supplied parameter.
             *
             * param: keyword (language).
             */
            void setLanguage(Lang language);

            /**
             * Set the list-options, based on the supplied parameter.
             *
             * param: keyword (NO* = off, otherwise on).
             */
            void setListOptions(KWD::Keyword keyword);

            /**
             * Set the optimization-level, based on the supplied parameter.
             *
             * param: level (0 | 1 | 2 | 3).
             */
            void setOptLevel(uint32_t opt) { optLevel = opt; }

            /**
             * Set the string switches, based on the supplied parameters.
             *
             * param: keyword
             * param: valueStr
             */
            void setString(KWD::Keyword keyword, std::string valueStr);

            /* GETTERS */

            /**
             * Get the on/off switch, based on the supplied parameter.
             *
             * returns: a pointer to the structure containing the settings
             *          for the on/off switches.
             */
            OnOffSwitches *getOnOff() { return (&onOff); }

            /**
             * Get the language-list, based on the supplied parameter.
             *
             * returns: a pointer to the vector containing the set of
             *          languages for which the module is being compiled.
             */
            std::vector<Lang> *getLangauge() { return (&languageList); }

            /**
             * Get the list-options currently in effect.
             *
             * returns: a pointer to the listing option settings.
             */
            ListOptions *getListOptions() { return (&list); }

            /**
             * Set the optimization-level, based on the supplied parameter.
             *
             * param: level (0 | 1 | 2 | 3).
             */
            uint32_t getOptLevel() { return (optLevel); }

            /**
             * Get the string switches.
             */
            std::string getIdent() { return (ident); }
            std::string getLinkage() { return (linkage); }
            std::string getMain() {return (main); }
            std::string getVersion() {return (version); }

        private:
            SwitchesDeclAST(SwitchesDeclAST const&) = delete;               // Prevent copy constructor
            SwitchesDeclAST& operator=(SwitchesDeclAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
            bool langListSet;
            bool linkageSet;

            /*
             * on-off switches
             */
            OnOffSwitches onOff;

            /*
             * special switches:common switches
             */
            std::vector<Lang> languageList;
            ListOptions list;
//            StructureRefAST structure;
            std::string ident;
            std::string linkage;
            std::string main;
            std::string version;
            uint32_t optLevel;

            /*
             * special switches:common switches:BLISS-32 switches
             * special switches:common switches:BLISS-64 switches
             */
            ModeSwitches mode;
    };

    /*
     * built-in-declaration --- BUILTIN built-in-name, ... ;
     *
     * built-in-name --- name
     */
    class BuiltInAST : public DeclarationAST
    {
        public:
            BuiltInAST() {}
            virtual ~BuiltInAST();

        private:
            BuiltInAST(BuiltInAST const&) = delete;               // Prevent copy constructor
            BuiltInAST& operator=(BuiltInAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * label-declaration --- LABEL label-name, ... ;
     *
     * label-name --- name
     */
    class LabelAST : public DeclarationAST
    {
        public:
            LabelAST() {}
            virtual ~LabelAST();

        private:
            LabelAST(LabelAST const&) = delete;               // Prevent copy constructor
            LabelAST& operator=(LabelAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };

    /*
     * undeclare-declaration --- UNDECLARE undeclare-name, ... ;
     *
     * undeclare-name --- name
     */
    class UndeclareAST : public DeclarationAST
    {
        public:
            UndeclareAST() {}
            virtual ~UndeclareAST();

        private:
            UndeclareAST(UndeclareAST const&) = delete;               // Prevent copy constructor
            UndeclareAST& operator=(UndeclareAST const&) = delete;    // Prevent assignment

            /* MEMBER DATA */
    };
}

#endif /* LLVM_BLISS_SPECIALINAST_H */
