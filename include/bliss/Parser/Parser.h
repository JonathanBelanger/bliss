/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Parser.h
 *
 * V01.000  16-Nov-2019     Jonathan D. Belanger
 * Initially written.
 */

#ifndef LLVM_BLISS_PARSER_H
#define LLVM_BLISS_PARSER_H

#include "Basic/CommonInclude.h"
#include "Basic/FileManager.h"
#include "Lexer/Lexer.h"
#include "Parser/Module.h"

namespace bliss
{
    class Parser
    {
        public:

            /**
             * This function is called to return the address of the one and only
             * Blocks Parser.  If one has not been instantiated, this call will do
             * so.  If one has been instantiated, then it is just returned.
             *
             * @param lex - A pointer to the lexer that will be used.
             * @return A pointer to the Parser class.
             */
            static Parser
            *get(Lexer *lex)
            {
                if (parser == nullptr)
                {
                    parser = new Parser(lex);
                }
                return (parser);
            }
            void handleModule(void) { return (module->handleModule()); }

        private:
            Parser(Lexer *lex);
            ~Parser();
            static Lexer *lexer;
            static Module *module;
            static Parser *parser;
    };
}

#endif /* LLVM_BLISS_PARSER_H */
