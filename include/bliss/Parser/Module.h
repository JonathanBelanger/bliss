/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Module.h
 *
 * V01.000  14-Sep-2019     Jonathan D. Belanger
 * Initially written.
 *
 * V01.001  16-Nov-2019     Jonathan D. Belanger
 * Separated the Parser.h and Parser.cpp files into Module, Blocks, and
 * Switches files, respectively.
 */

#ifndef LLVM_BLISS_MODULE_H
#define LLVM_BLISS_MODULE_H

#include "Lexer/Lexer.h"
#include "AST/ExprAST.h"
#include "AST/PrimaryAST.h"
#include "AST/OperatorAST.h"
#include "AST/ExecutableFuncAST.h"
#include "AST/ControlAST.h"
#include "AST/ModuleAST.h"
#include "Parser/Blocks.h"
#include "Parser/Switches.h"

namespace bliss
{
    class Module
    {
        friend class Parser;

        public:
            /*
             * The top-level structure is the MODULE, which is terminated by ELUDOM.
             */
            void
            handleModule();

            std::unique_ptr<ModuleAST>
            handleModuleHead();

            std::unique_ptr<BlockAST>
            handleModuleBody();

            std::unique_ptr<SwitchesDeclAST>
            handleModuleSwitches();

            /* CONSTRUCTORS */
            Module(Lexer *lex)                          // Private constructor
            {
                lexer = lex;
                switches = new Switches(lex);
                blocks = new Blocks(lex);
            };
            Module(Module const&) = delete;             // Prevent copy constructor
            Module& operator=(Module const&) = delete;  // Prevent assignment

            /* DESTRUCTORS */
            ~Module()
            {
                delete switches;
                delete blocks;
                switches = nullptr;
                blocks = nullptr;
            };

        private:
            static Lexer *lexer;
            static Switches *switches;
            static Blocks *blocks;
    };
}

#endif /* LLVM_BLISS_MODULE_H */
