/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Parser.h
 * Author: Jonathan D. Belanger
 *
 * V01.000  14-Sep-2019     Jonathan D. Belanger
 * Initially written.
 *
 * V01.001  16-Nov-2019     Jonathan D. Belanger
 * Separated the Parser.h and Parser.cpp files into Module, Blocks, and
 * Switches files, respectively.
 */

#ifndef LLVM_BLISS_SWITCHES_H
#define LLVM_BLISS_SWITCHES_H

#include "Lexer/Lexer.h"
#include "AST/DeclarationAST.h"
#include "AST/SpecialAST.h"

namespace bliss
{
    class Switches
    {
        friend class Module;

        public:

            /*
             * The top-level structure is the MODULE, which is terminated by ELUDOM.
             */
            std::unique_ptr<SwitchesDeclAST>
            handleSwitches(bool moduleLevel = false);

            std::unique_ptr<StructureAttrAST>
            handleStructureOption();

            std::string
            handleIdent();

            SwitchesDeclAST::Lang
            handleLanguage();

            std::string
            handleLinkage();

            KWD::Keyword
            handleListOption();

            std::string
            handleMain();

            uint32_t
            handleOptLevel();

            std::string
            handleVersion();

            /* CONSTRUCTORS */
            Switches(Lexer *lex){ lexer = lex; }            // Private constructor
            Switches(Switches const&) = delete;             // Prevent copy constructor
            Switches& operator=(Switches const&) = delete;  // Prevent assignment

        private:
            static Lexer *lexer;
    };
}

#endif /* LLVM_BLISS_SWITCHES_H */
