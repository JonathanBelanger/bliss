/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Blocks.h
 * Author: Jonathan D. Belanger
 *
 * V01.000  14-Sep-2019     Jonathan D. Belanger
 * Initially written.
 *
 * V01.001  16-Nov-2019     Jonathan D. Belanger
 * Separated the Parser.h and Parser.cpp files into Module, Blocks, and
 * Switches files, respectively.
 */

#ifndef LLVM_BLISS_BLOCKS_H
#define LLVM_BLISS_BLOCKS_H

#include "Lexer/Lexer.h"
#include "AST/BlockAST.h"

namespace bliss
{
    class Blocks
    {
        friend class Module;

        public:

            /*
             * Blocks are started by either a BEGIN keyword or a '(', and ended
             * with END or ')', respectively.
             */
            std::unique_ptr<BlockAST>
            handleBlock(bool moduleLevel = false);

            /* CONSTRUCTORS */
            Blocks(Lexer *lex) { lexer = lex; }         // Private constructor
            Blocks(Blocks const&) = delete;             // Prevent copy constructor
            Blocks& operator=(Blocks const&) = delete;  // Prevent assignment

            /* DESTRUCTORS */
            ~Blocks() { lexer = nullptr; }

        private:
            static Lexer *lexer;
    };
}

#endif /* LLVM_BLISS_BLOCKS_H */
