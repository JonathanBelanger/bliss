/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Error.h
 * Author: Jonathan D. Belanger
 *
 * Created on December 7, 2019, 03:36 PM
 */
#ifndef INCLUDE_BLISS_BASIC_ERROR_H_
#define INCLUDE_BLISS_BASIC_ERROR_H_

#include "Basic/CommonInclude.h"

namespace bliss
{
    class Error
    {
        public:
            typedef enum _severity_level
            {
                WARNING,
                SUCCESS,
                ERROR,
                INFO,
                FATAL
            } SeverityLevel;

            /* CONSTRUCTOR */
            Error(std::string message, SeverityLevel level, uint32_t start, uint32_t end) :
                severity(level), column_start(start)
            {
                size_t first = message.find_first_not_of(' ');
                size_t last = message.find_last_not_of(' ');

                if (std::string::npos == first)
                {
                    first = 0;
                }
                text = message.substr(first, (last - first + 1));
                if (column_start == 0)
                {
                    column_end = 0;
                }
                else
                {
                    column_end = end;
                }
            }

            /* DESTRUCTOR */
            ~Error() {}

            /* GETTERS */

            /**
             * This function is called to get a formatted message to display into the listing output.
             *
             * @param message   - This is a formatted string.  If the column start and end values were given, then
             *                    there will be second line containing a string that indicates the start and end
             *                    locations where the error was detected.
             * @param leadingSpaces - This is an integer used to indicate the number of spaces that should be added to
             *                        the beginning of the error position indicator.
             * @return - a value indicating the number of lines returned in the number of output lines in the returned\
             *           message parameter.
             */
            uint32_t
            getString(std::string &message, uint32_t leadingSpaces);

        private:
            Error(Error const&) = delete;                   // Prevent copy constructor
            Error& operator=(Error const&) = delete;        // Prevent assignment
            std::string text;
            SeverityLevel severity;
            uint32_t column_start, column_end;
    };
}

#endif /* INCLUDE_BLISS_BASIC_ERROR_H_ */
