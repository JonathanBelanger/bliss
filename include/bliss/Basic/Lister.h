/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   Lister.h
 * Author: Jonathan D. Belanger
 *
 * Created on October 6, 2019, 10:02 AM
 */
#ifndef LLVM_BLISS_LISTER_H
#define LLVM_BLISS_LISTER_H

#include "Basic/CommonInclude.h"
#include "Basic/Error.h"

namespace bliss
{
    class Lister
    {
        public:

            /**
             * This function is called to return the address of the one and only Lister.
             * If one has not been instantiated, this call will do so.  If one has been
             * instantiated, then it is just returned.
             *
             * @return A pointer to the Parser class.
             */
            static Lister
            *get()
            {
                if (lister == nullptr)
                {
                    lister = new Lister();
                }
                return(lister);
            }

            /**
             * Open a listing file.  This must be done before any input files are read,
             * if we are generating a listing.  There is only 1 listing file for each
             * input file specified on the command line.  REQUIRE files may be output
             * in a listing file, but in the listed file for the top most parent input
             * file containing the REQUIRE statement.
             *
             * @param listingFile - a string specifying the listing file to be written.
             * @return true - the listing file has been successfully opened for write.
             *         false - a failure occurred when attempting to open the listing
             *                 file for write.
             */
            bool
            open(std::string listingFile);

            /**
             * Close an opened listing file.
             */
            void
            close(void);

            /**
             * This function is called to increment the depth.  The depth is incremented
             * when starting a new block.
             */
            void
            incrDepth() { depthDirection++; return; };

            /**
             * This function is called to decrement the depth.  The depth is decremented
             * when ending a block.
             */
            void
            decrDepth() { depthDirection--; return; };

            /**
             * This function is called to update the depth based on the current value of
             * the direction.  This is called after the current depth has been displayed.
             * We do this to be consistent with the way the BLISS-16, BLISS-32, BLISS_36,
             * and BLISS-64 compilers worked.
             */
            void
            updateDepth()
            {
                if ((depthDirection < 0) && (std::abs(depthDirection) > depth))
                {
                    depth = 0;
                }
                else
                {
                    depth += depthDirection;
                }
                depthDirection = 0;
            }

            /**
             * This function is called to add a new character to the end of the current
             * listingLine.
             *
             * @param c - An ASCII character value to be added to the listing file.
             */
            void
            addChar(char c);

            /**
             * This function is called to add a message to be output with the
             * next line.  The message vector will be reset after each source
             * line is written out.  If start is specified as 0, then no
             * column indicator will be displayed and end is ignored.  If
             * start is specified >0, then if the column indicator is 0, then
             * only the start column will be indicated.  Otherwise, both will
             * be indicated.
             *
             * @param msg - A message string.
             * @param sev - A Severity Level for the message.
             * @param start - A value indicating the starting column of code
             *                that generated the message.
             * @param end - A value indicating the ending column of code that
             *              generated the message.
             */
            void
            addMessage(std::string msg, Error::SeverityLevel sev, uint32_t start, uint32_t end);

        private:

            /* CONSTRUCTORS */
            Lister()                                    // Private constructor
            {
                listingStream = &std::cout;
                depth = 0;
                currentLine = currentColumn = 1;
                pageLine = 0;
                listingLine = "";
                newLineTrigger = '\0';
                depthDirection = 0;
                return;
            };
            Lister(Lister const&) = delete;             // Prevent copy constructor
            Lister& operator=(Lister const&) = delete;  // Prevent assignment

            /* DESTRUCTORS */
            ~Lister(){};

            /* CLASS DATA */
            static Lister *lister;
            std::ostream *listingStream;
            std::vector<Error *> messages;
            uint32_t depth;
            uint32_t currentLine;
            uint32_t currentColumn;
            uint32_t pageLine;
            std::string listingLine;
            char newLineTrigger;
            int32_t depthDirection;
    };
}

typedef struct listOptions
{
    bool source;
    bool require;
    bool expand;
    bool trace;
    bool library;
    bool object;
    bool assembly;
    bool symbolic;
    bool binary;
    bool commentary;
} ListOptions;

#endif /* LLVM_BLISS_LISTER_H */
