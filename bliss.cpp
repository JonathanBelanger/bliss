/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * File:   bliss.cpp
 * Author: Jonathan D. Belanger
 *
 *  V01.000 Jonathan D. Belanger    August 31, 2019, 3:02 PM
 *  Initially created.
 *
 *  V01.001 Jonathan D. Belanger    September 1, 2019, 1:55 PM
 *  Introduced the following classes:
 *      InputChar - contains information about a single character, including
 *                  its character class.
 *      InputFile - contains the code to read a single character from a file
 *                  and return it back to the caller.  Also, handles character
 *                  classification and end of file detection.
 *      FileManager - contains the code to be able to open multiple files and
 *                  close them in the reverse order in which they were opened.
 *  The next step will be to move these into their own include and module
 *  files.
 *
 *  V01.002 Jonathan D. Belanger    December 21, 2019 8:35 AM
 *  This is being written a bit late, but the actual processing of input source
 *  files has been moved out to the Driver module.  All this module does is
 *  read in the input source files and sends them to the Driver for processing.
 *  In the future, this module will also be responsible for processing command
 *  line arguments and them up for the driver to process.
 */
#include "Driver/Driver.h"

using namespace bliss;

Driver *Driver::driver = nullptr;

/*
 * This function is called to test the input file processing and character
 * classification code.
 */
int main(int argc, char** argv)
{
    Driver *driver = Driver::get();
    std::vector<std::string> files =
    {
        "/home/belanger/Projects/bliss/tests/AST/module_test_01.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_02.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_03.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_04.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_05.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_06.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_07.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_08.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_09.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_10.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_11.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_12.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_13.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_14.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_15.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_16.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_17.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_18.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_19.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_20.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_21.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_22.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_23.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_24.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_25.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_26.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_27.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_28.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_29.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_30.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_31.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_32.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_33.b64",
        "/home/belanger/Projects/bliss/tests/AST/module_test_34.b64"
    };

    if (driver->setFiles(&files) == true)
    {
        driver->mainLoop();
    }
    return (0);
}
