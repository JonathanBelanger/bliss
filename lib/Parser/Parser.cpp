/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * V01.000  17-Nov-2019     Jonathan D. Belanger
 * Initially written.
 */
#include "Parser/Parser.h"

/*
 * DESIGN CONCEPT:
 *
 *  When parsing lexemes the following expectations have been applied:
 *
 *      1)  When a lexeme indicates a next level in parsing, the lexeme will
 *          remain in the Lexer and the next level parsing routine called.  Do
 *          not call Lexer::getNext.  This is done because the next level
 *          parser may need this information to be able to perform its
 *          processing.  For example, BEGIN..END versus (..), which are
 *          equivalent but cannot be be used to start or end a block when the
 *          other was used (this is a syntax error).
 *      2)  Within parsing routines, an infinite loop is utilized, where the
 *          last statement in the loop is Lexer::getNext().  Lexer::getNext()
 *          should only be called at the end of the loop.  We cannot do it at
 *          the top because of expectation #1.
 *      3)  When the end of parsing a level is detected, the lexeme that
 *          indicates this will remain in the Lexer and a return with any data
 *          performed back to the caller.  The caller will handle moving onto
 *          the next lexeme.  See expectations #1 and #2.
 */

using namespace bliss;

Lexer *Parser::lexer = nullptr;
Module *Parser::module = nullptr;

/**
 * Constructor for the Parser class.  This will also create the Module class
 * used by the parse.
 *
 * @param: lex - A pointer to the lexer class.
 */
Parser::Parser(Lexer *lex)
{
    lexer = lex;
    module = new Module(lex);
}

/**
 * Destructor for the Parser class.  This will also delete the Module class
 * used by the parse.
 */
Parser::~Parser()
{
    delete module;
    module = nullptr;
    lexer = nullptr;
    parser = nullptr;
}

