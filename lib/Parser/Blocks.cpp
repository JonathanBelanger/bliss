/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * V01.000  14-Sep-2019     Jonathan D. Belanger
 * Initially written.
 *
 * V01.001  16-Nov-2019     Jonathan D. Belanger
 * Separated the Parser.h and Parser.cpp files into Module, Blocks, and
 * Switches files, respectively.
 */
#include "Parser/Blocks.h"

/*
 * DESIGN CONCEPT:
 *
 *  When parsing lexemes the following expectations have been applied:
 *
 *      1)  When a lexeme indicates a next level in parsing, the lexeme will
 *          remain in the Lexer and the next level parsing routine called.  Do
 *          not call Lexer::getNext.  This is done because the next level
 *          parser may need this information to be able to perform its
 *          processing.  For example, BEGIN..END versus (..), which are
 *          equivalent but cannot be be used to start or end a block when the
 *          other was used (this is a syntax error).
 *      2)  Within parsing routines, an infinite loop is utilized, where the
 *          last statement in the loop is Lexer::getNext().  Lexer::getNext()
 *          should only be called at the end of the loop.  We cannot do it at
 *          the top because of expectation #1.
 *      3)  When the end of parsing a level is detected, the lexeme that
 *          indicates this will remain in the Lexer and a return with any data
 *          performed back to the caller.  The caller will handle moving onto
 *          the next lexeme.  See expectations #1 and #2.
 */

using namespace bliss;

Lexer *Blocks::lexer = nullptr;

/*
 * This function is responsible for parsing the block.  A block is either
 * labeled or unlabeled.
 *
 *  block -+- labeled-block
 *         +- unlabeled-block
 *
 * labeled-block --- {label: }... unlabeled-block
 *
 * unlabeled-block -+- BEGIN block-body END
 *                  +- ( block-body )
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @param module-level - a boolean flag indicating that this is at the module
 *                       level or not.
 *
 * @return pointer to BlockAST.
 */
std::unique_ptr<BlockAST>
Blocks::handleBlock(bool moduleLevel)
{
    bool beginFound = lexer->getType() == Lexer::LTKeyword;
    auto block = llvm::make_unique<BlockAST>();

    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                switch (lexer->getKeyword())
                {
                    case KWD::BEGIN:
                    {
                        break;
                    }

                    case KWD::END:
                        if (beginFound == false)
                        {
                            std::cerr << "module block started with ( terminated with END\n";
                            return (nullptr);
                        }
                        else
                        {
                            return (std::move(block));
                        }

                    default:
                        std::cerr << "unexpected keyword found, " << lexer->getString() << "\n";
                        return (nullptr);
                }
                break;

            case Lexer::LTPunctuation:
                switch (lexer->getPunct())
                {
                    case Lexer::PunctLParen:
                    {
                        break;
                    }

                    case Lexer::PunctRParen:

                        /*
                         * Eat the ')' before returning to the caller.
                         */
                        if (beginFound)
                        {
                            std::cerr << "module block started with BEGIN terminated with ')'\n";
                            return (nullptr);
                        }
                        else
                        {
                            return (std::move(block));
                        }

                    default:
                        std::cerr << "Unexpected punctuation, '" << lexer->getChar() <<
                                     "', at start of module switches\n";
                        break;
                }
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got and unexpected lexical: " << lexer->getType() << "\n";
                return (nullptr);
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return (nullptr);
        }
    }
    return (nullptr);
}
