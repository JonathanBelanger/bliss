/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * V01.000  14-Sep-2019     Jonathan D. Belanger
 * Initially written.
 *
 * V01.001  16-Nov-2019     Jonathan D. Belanger
 * Separated the Parser.h and Parser.cpp files into Module, Blocks, and
 * Switches files, respectively.
 */
#include "Parser/Module.h"

/*
 * DESIGN CONCEPT:
 *
 *  When parsing lexemes the following expectations have been applied:
 *
 *      1)  When a lexeme indicates a next level in parsing, the lexeme will
 *          remain in the Lexer and the next level parsing routine called.  Do
 *          not call Lexer::getNext.  This is done because the next level
 *          parser may need this information to be able to perform its
 *          processing.  For example, BEGIN..END versus (..), which are
 *          equivalent but cannot be be used to start or end a block when the
 *          other was used (this is a syntax error).
 *      2)  Within parsing routines, an infinite loop is utilized, where the
 *          last statement in the loop is Lexer::getNext().  Lexer::getNext()
 *          should only be called at the end of the loop.  We cannot do it at
 *          the top because of expectation #1.
 *      3)  When the end of parsing a level is detected, the lexeme that
 *          indicates this will remain in the Lexer and a return with any data
 *          performed back to the caller.  The caller will handle moving onto
 *          the next lexeme.  See expectations #1 and #2.
 */

using namespace bliss;

Lexer *Module::lexer = nullptr;
Blocks *Module::blocks = nullptr;
Switches *Module::switches = nullptr;

/*
 * The top-level structure is the MODULE, which is terminated by ELUDOM.  This
 * structure contains five items that need to be parsed.  These items are:
 *
 *  - MODULE
 *  - module-head
 *  - '='
 *  - module-body
 *  - ELUDOM
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 *  module --- MODULE module-head = module-body ELUDOM
 */
void
Module::handleModule()
{
    bool atStart = true;

    while(true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                switch (lexer->getKeyword())
                {
                    case KWD::MODULE:
                        if (atStart)
                        {
                            atStart = false;
                        }
                        else
                        {
                            std::cerr << "Unexpected keyword, " << lexer->getString() << "\n";
                        }
                        break;

                    case KWD::ELUDOM:
                        return;

                    default:
                        std::cerr << "Unexpected keyword, " << lexer->getString() << "\n";
                        return;
                }
                break;

            case Lexer::LTExplicitDeclared:
            {
                auto module = handleModuleHead();
                if (module != nullptr)
                {
                    std::cerr << "Failed to parse module-head, which is OK.\n";
                    // return;
                }
                break;
            }

            case Lexer::LTOperator:
                if (lexer->getOper() == Lexer::OperEqual)
                {
                    auto block = handleModuleBody();
                    if (block == nullptr)
                    {
                        std::cerr << "Failed to parse module-body\n";
                    }
                }
                else
                {
                    std::cerr << "Unexpected operator, '" << lexer->getChar() << "'\n";
                }
                break;

            case Lexer::LTPunctuation:
                if (lexer->getPunct() == Lexer::PunctLParen)
                {
                    auto switches = handleModuleSwitches();
                    if (switches != nullptr)
                    {
                        // TODO: Need to finish this out.
                    }
                }
                else
                {
                    std::cerr << "Unexpected punctuation, '" << lexer->getChar() << "' (1)\n";
                    return;
                }
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Unexpected lexeme " << lexer->getType() << "\n";
                return;
        }

        /*
         * Get the next lexeme.
         */
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file\n";
            return;
        }
    }
}

/*
 * This function is responsible for parsing the module-head.  The module-head
 * consists of 2 items.  A module-head contains:
 *
 *  module-name
 *  module-switches     <--- this will be handled in the caller
 *
 *  module-head --- module-name --- module-switches
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @return pointer to ModuleAST.
 */
std::unique_ptr<ModuleAST>
Module::handleModuleHead()
{
    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTExplicitDeclared:
                {
                    auto module = llvm::make_unique<ModuleAST>(lexer->getString());
                    if (module)
                    {
                        return (std::move(module));
                    }
                    else
                    {
                        std::cerr << "Failed to allocate a ModuleAST\n";
                        return (nullptr);
                    }
                }
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Expected module-name\n";
                return (nullptr);
        }

        /*
         * Get the next lexeme.
         */
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file\n";
            return (nullptr);
        }
    }
}

/*
 * This function is responsible for parsing the module-body.  The module-body
 * contains one item and it is just a block.  A block must begin with either a
 * BEGIN keyword or an open parenthesis, '('.
 *
 *  module-body --- block
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @return pointer to BlockAST.
 */
std::unique_ptr<BlockAST>
Module::handleModuleBody()
{
    bool atStart = true;

    while(true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTOperator:
                if ((lexer->getOper() == Lexer::OperEqual) && (atStart == true))
                {
                    atStart = false;
                }
                else
                {
                    std::cerr << "Unexpected operator, " << lexer->getChar() << ", detected.\n";
                }
                break;

            case Lexer::LTPunctuation:
                if (lexer->getPunct() == Lexer::PunctLParen)
                {
                    return (blocks->handleBlock(true));
                }
                else
                {
                    std::cerr << "Got unexpected punctuation, '" << lexer->getChar() << "' (2)\n";
                    return (nullptr);
                }
                break;

            case Lexer::LTKeyword:
                if (lexer->getKeyword() == KWD::BEGIN)
                {
                    return (blocks->handleBlock(true));
                }
                else
                {
                    std::cerr << "Got an unexpected keyword, " << lexer->getString() << "\n";
                    return (nullptr);
                }
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got an unexpected lexical: " << lexer->getType() << "\n";
                return (nullptr);
        }
        if (lexer->getNext() == false)
        {
            return (nullptr);
        }
    }
    return (nullptr);
}

/*
 * This function is responsible for parsing the module-switches.  It does not
 * actually process the module switch information, but does process all of the
 * module-switch information between an open parenthesis '(' and a close ')'.
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @return pointer to SwitchesDeclAST.
 */
std::unique_ptr<SwitchesDeclAST>
Module::handleModuleSwitches()
{

    /*
     * All we really need to do is call the code that handles the general
     * switch statement and indicate that it be being called at the module
     * level.
     */
    return (switches->handleSwitches(true));
}
