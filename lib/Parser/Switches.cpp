/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * V01.000  14-Sep-2019     Jonathan D. Belanger
 * Initially written.
 *
 * V01.001  16-Nov-2019     Jonathan D. Belanger
 * Separated the Parser.h and Parser.cpp files into Module, Blocks, and
 * Switches files, respectively.
 */
#include "Parser/Switches.h"

/*
 * DESIGN CONCEPT:
 *
 *  When parsing lexemes the following expectations have been applied:
 *
 *      1)  When a lexeme indicates a next level in parsing, the lexeme will
 *          remain in the Lexer and the next level parsing routine called.  Do
 *          not call Lexer::getNext.  This is done because the next level
 *          parser may need this information to be able to perform its
 *          processing.  For example, BEGIN..END versus (..), which are
 *          equivalent but cannot be be used to start or end a block when the
 *          other was used (this is a syntax error).
 *      2)  Within parsing routines, an infinite loop is utilized, where the
 *          last statement in the loop is Lexer::getNext().  Lexer::getNext()
 *          should only be called at the end of the loop.  We cannot do it at
 *          the top because of expectation #1.
 *      3)  When the end of parsing a level is detected, the lexeme that
 *          indicates this will remain in the Lexer and a return with any data
 *          performed back to the caller.  The caller will handle moving onto
 *          the next lexeme.  See expectations #1 and #2.
 */

using namespace bliss;

Lexer *Switches::lexer = nullptr;

/*
 * This function is responsible for parsing the list of module-switches.  It
 * does not actually process the switch information, but does process all of the
 * module-switch information between an open parenthesis '(' and a close ')'.
 * The module-switch consists of 3 items.  A module-switch contains:
 *
 *  (
 *  module-switch, ...
 *  )
 *
 *  module-switches -+- ( module-switch, ... )
 *                   +- nothing
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @param module-level - a boolean flag indicating that this is at the module
 *                       level or not.
 *
 * @return pointer to SwitchesDeclAST.
 */
std::unique_ptr<SwitchesDeclAST>
Switches::handleSwitches(bool moduleLevel)
{
    bool atStart = false;
    KWD::Keyword keyword;
    std::string valueStr;
    uint32_t value;
    auto switches = llvm::make_unique<SwitchesDeclAST>();
    SwitchesDeclAST::Lang lang;

    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTPunctuation:
                switch (lexer->getPunct())
                {
                    case Lexer::PunctLParen:
                        if (atStart == false)
                        {
                            atStart = true;
                        }
                        else
                        {
                            std::cerr << "Too many opening parentheses, '(', " <<
                                         "at start of module-switches\n";
                        }
                        break;

                    case Lexer::PunctRParen:
                        if (atStart == true)
                        {
                            return (std::move(switches));
                        }
                        else
                        {
                            std::cerr << "Missing open parenthesis, (, at start " <<
                                         "of module-switches\n";
                        }
                        break;

                    case Lexer::PunctComma:
                        break;

                    default:
                        std::cerr << "Unexpected punctuation, '" << lexer->getChar() << "' (3)\n";
                        break;
                }
                break;

            case Lexer::LTKeyword:
                keyword = lexer->getKeyword();
                switch (keyword)
                {
                    case KWD::CODE:         case KWD::NOCODE:
                    case KWD::DEBUG:        case KWD::NODEBUG:
                    case KWD::ERRS:         case KWD::NOERRS:
                    case KWD::OPTIMIZE:     case KWD::NOOPTIMIZE:
                    case KWD::SAFE:         case KWD::NOSAFE:
                    case KWD::UNAMES:       case KWD::NOUNAMES:
                    case KWD::ZIP:          case KWD::NOZIP:
                        switches->setOnOff(lexer->getKeyword());
                        break;

                    case KWD::IDENT:
                        switches->setString(keyword, handleIdent());
                        break;

                    case KWD::LANGUAGE:
                        lang = handleLanguage();
                        while (lang != SwitchesDeclAST::LANG_DEFAULT)
                        {
                            switches->setLanguage(lang);
                            lang = handleLanguage();
                        }
                        break;

                    case KWD::LINKAGE:
                        switches->setString(keyword, handleLinkage());
                        break;

                    case KWD::LIST:
                        keyword = handleListOption();
                        while (keyword != KWD_MAX)
                        {
                            switches->setListOptions(keyword);
                            keyword = handleListOption();
                        }
                        break;

                    case KWD::STRUCTURE:
                        {
                            auto structAttr = handleStructureOption();
                            if (structAttr == nullptr)
                            {
                                std::cerr << "Failed to allocate a StructureAttrAST, which is OK\n";
                            }
                        }
                        break;

                    case KWD::MAIN:
                        valueStr = handleMain();
                        switches->setString(keyword, valueStr);
                        break;

                    case KWD::OPTLEVEL:
                        value = handleOptLevel();
                        switches->setOptLevel(value);
                        break;

                    case KWD::VERSION:
                        valueStr = handleVersion();
                        switches->setString(keyword, valueStr);
                        break;

                    default:
                        std::cerr << "Unrecognized keyword detected, " << lexer->getString() << "\n";
                        break;
                }
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got and unexpected lexical: " << lexer->getType() << "\n";
                return (nullptr);
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return (nullptr);
        }
    }
    return (nullptr);
}

/*
 * This function is responsible for parsing the STRUCTURE switch.  The
 * STRUCTURE switch consists of 4 items.  A STRUCTURE switch contains:
 *
 *  STRUCTURE
 *  (
 *  structure-attribute |  nothing
 *  )
 *
 *  STRUCTURE ---( -+- structure-attribute -+- )
 *                  +- nothing -------------+
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @return pointer to StructureAttrAST.
 */
std::unique_ptr<StructureAttrAST>
Switches::handleStructureOption()
{
    int32_t state = -1;

    /*
     * The state is used to determine when we are processing a STRUCTURE keyword or not.
     * A -1 is used to indicate if we are in the processing of a STRUCTURE or not.  Once
     * the STRUCTURE has been detected, then the state is set to 0 and is now used to
     * count the number of open parenthesis '(' without a paired close ')'.  When the
     * state reaches 0, we have closed the last pair of parenthesis and can return back
     * to the caller.  When the state is greater than zero, all keywords and other values
     * will be accepted and eaten.
     */
    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                if (lexer->getKeyword() == KWD::STRUCTURE)
                {
                    if (state == -1)
                    {
                        state = 0;
                    }
                    else
                    {
                        std::cerr << "Too many STRUCTURE keywords detected (" << state <<")\n";
                        return (nullptr);
                    }
                }
                else if (state == 0)
                {
                    std::cerr << "Keyword detected, " << lexer->getString() <<
                                 ", prior to open parenthesis of STRUCTURE (" << state <<")\n";
                    return (nullptr);
                }
                break;

            case Lexer::LTPunctuation:
                switch (lexer->getPunct())
                {
                    case Lexer::PunctLParen:
                        state++;
                        if (state > 1)
                        {
                            std::cerr << "module-switch common-switch STRUCTURE not implemented";
                        }
                        break;

                    case Lexer::PunctRParen:
                        state--;
                        if (state == 0)
                        {
                            return (nullptr);
                        }
                        break;

                    default:
                        if (state <= 0)
                        {
                            std::cerr << "Unrecognized punctuation detected, " << lexer->getChar() << "\n";
                            return (nullptr);
                        }
                        break;
                }
                break;

            case Lexer::LTTrailingComment:
            case Lexer::LTEmbeddedComment:
                break;

            default:
                if (state <= 0)
                {
                    std::cerr << "Got and unexpected lexical (9): " << lexer->getType() << "\n";
                    return (nullptr);
                }
                break;
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return (nullptr);
        }
    }
    return (nullptr);
}

/*
 * This function is responsible for parsing the IDENT switch.  The IDENT
 * switch consists of 3 items.  An IDENT switch contains:
 *
 *  IDENT
 *  =
 *  quoted-string
 *
 *  IDENT = quoted-string
 *
 * In addition, there can be any number of comments, both trailing and
 * embedded.  That need to be handled as well.
 *
 * @return a pointer to the quoted string as just a string.
 */
std::string
Switches::handleIdent()
{
    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                if (lexer->getKeyword() != KWD::IDENT)
                {
                    std::cerr << "Unrecognized keyword, '" << lexer->getString() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTOperator:
                if (lexer->getOper() != Lexer::OperEqual)
                {
                    std::cerr << "Unrecognized operator in IDENT, '" <<
                            lexer->getChar() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTQuotedString:
                return (lexer->getString());
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got and unexpected lexical: " << lexer->getType() << "\n";
                return ("");
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return ("");
        }
    }
    return ("");
}

/*
 * This function is responsible for parsing the LINKAGE switch.  The
 * LINKAGE switch consists of 4 items.  A LINKAGE switch contains:
 *
 *  LINKAGE
 *  (
 *  name
 *  )
 *
 *  LINKAGE ( name )
 *
 * @return the name as a string.
 */
std::string
Switches::handleLinkage()
{
    bool openPresent = false;
    std::string retVal = "";

    retVal.clear();
    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                if (lexer->getKeyword() != KWD::LINKAGE)
                {
                    std::cerr << "Unrecognized keyword, '" << lexer->getString() << "'\n";
                    return (retVal);
                }
                break;

            case Lexer::LTPunctuation:
                if (lexer->getPunct() == Lexer::PunctLParen)
                {
                    if (openPresent == true)
                    {
                        return (retVal);
                    }
                }
                else if (lexer->getPunct() == Lexer::PunctRParen)
                {
                    if (openPresent == true)
                    {
                        return (retVal);
                    }
                    else
                    {
                        return (retVal);    // TODO: Need to figure something out here.
                    }
                }
                else
                {
                    std::cerr << "Unrecognized punctuation in LINKAGE, '" <<
                            lexer->getChar() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTExplicitDeclared:
                retVal = lexer->getString();
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got an unexpected lexical: " << lexer->getType() << "\n";
                return (retVal);
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return (retVal);
        }
    }
    return (retVal);
}

/*
 * This function is responsible for parsing the LIST option switch.
 * This function is called repeatedly until the last list-option item
 * has been returned.  The LIST OPTION switch consists of 4 items.
 * An LIST option switch contains:
 *
 *  LIST
 *  (
 *  list-option, ...
 *  )
 *
 *  LIST ( list-option, ... )
 *
 *  @return the next keyword in the list of list options.
 */
KWD::Keyword
Switches::handleListOption()
{
    static bool inList = false;
    KWD::Keyword retVal = KWD_MAX;

    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                switch (lexer->getKeyword())
                {
                    case KWD::LIST:
                        if (inList == true)
                        {
                            std::cerr << "LIST not a valid keyword in a list-option, ignoring\n";
                            retVal = KWD_MAX;
                        }
                        break;

                    case KWD::SOURCE:
                    case KWD::REQUIRE:
                    case KWD::EXPAND:
                    case KWD::TRACE:
                    case KWD::LIBRARY:
                    case KWD::OBJECT:
                    case KWD::ASSEMBLY:
                    case KWD::SYMBOLIC:
                    case KWD::BINARY:
                    case KWD::COMMENTARY:
                    case KWD::NOSOURCE:
                    case KWD::NOREQUIRE:
                    case KWD::NOEXPAND:
                    case KWD::NOTRACE:
                    case KWD::NOLIBRARY:
                    case KWD::NOOBJECT:
                    case KWD::NOASSEMBLY:
                    case KWD::NOSYMBOLIC:
                    case KWD::NOBINARY:
                    case KWD::NOCOMMENTARY:
                        retVal = lexer->getKeyword();
                        break;

                    default:
                        std::cerr << "Unexpected keyword, '" << lexer->getString() << "', in list-option\n";
                        return (KWD_MAX);
                }
                break;

            case Lexer::LTPunctuation:
                switch (lexer->getPunct())
                {
                    case Lexer::PunctLParen:
                        if (inList == false)
                        {
                            inList = true;
                        }
                        else
                        {
                            std::cerr << "Open parenthesis with list keyword was expected\n";
                            return (KWD_MAX);
                        }
                        break;

                    case Lexer::PunctRParen:
                        if (inList == true)
                        {
                            if (retVal == KWD_MAX)
                            {
                                inList = false;
                            }
                            return (retVal);
                        }
                        else
                        {
                            std::cerr << "Unexpected close parenthesis in list-option\n";
                            return (KWD_MAX);
                        }
                        break;

                    case Lexer::PunctComma:
                        lexer->getNext();   // Swallow the comma separator.
                        return (retVal);
                        break;

                    default:
                        std::cerr << "Unexpected punctuation, '" << lexer->getChar() << "' in list-option\n";
                        return (KWD_MAX);
                }
                break;

            default:
                std::cerr << "Got an unexpected lexical: " << lexer->getType() << "\n";
                return (KWD_MAX);
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return (KWD_MAX);
        }
    }
    return (retVal);
}

/*
 * This function is responsible for parsing the LANGUAGE switch.  The
 * LANGUAGE switch consists of 4 items.  A LANGUAGE switch contains:
 *
 *  LANGUAGE
 *  (
 *  language-list, ...
 *  )
 *
 *  LANGUAGE ( language-list, ... )
 *
 *                 +- COMMON
 *  language-list -+  language-name, ...
 *                 +- nothing
 *
 *
 * @return the language enumerated value.
 */
SwitchesDeclAST::Lang
Switches::handleLanguage()
{
    static bool inList = false;
    SwitchesDeclAST::Lang retVal = SwitchesDeclAST::LANG_DEFAULT;

    while (retVal == SwitchesDeclAST::LANG_DEFAULT)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                switch (lexer->getKeyword())
                {
                    case KWD::LANGUAGE:
                        if (inList == true)
                        {
                            std::cerr << "LANGUAGE not a valid keyword in a language-list\n";
                            inList = false;
                            return (SwitchesDeclAST::LANG_DEFAULT);
                        }
                        break;

                    case KWD::COMMON:
                        retVal = SwitchesDeclAST::LANG_COMMON;
                        break;

                    case KWD::BLISS32:
                        retVal = SwitchesDeclAST::LANG_BLISS32;
                        break;

                    case KWD::BLISS64:
                        retVal = SwitchesDeclAST::LANG_BLISS64;
                        break;

                    default:
                        std::cerr << "Unexpected keyword, '" << lexer->getString() << "', in language-list\n";
                        inList = false;
                        return (SwitchesDeclAST::LANG_DEFAULT);
                }
                break;

            case Lexer::LTPunctuation:
                switch (lexer->getPunct())
                {
                    case Lexer::PunctLParen:
                        if (inList == false)
                        {
                            inList = true;
                        }
                        else
                        {
                            std::cerr << "Open parenthesis with language keyword was expected\n";
                            inList = false;
                            return (SwitchesDeclAST::LANG_DEFAULT);
                        }
                        break;

                    case Lexer::PunctRParen:
                        if (inList == true)
                        {
                            if (retVal == SwitchesDeclAST::LANG_DEFAULT)
                            {
                                inList = false;
                            }
                            return (retVal);
                        }
                        else
                        {
                            std::cerr << "Unexpected close parenthesis in language-list\n";
                            inList = false;
                            return (SwitchesDeclAST::LANG_DEFAULT);
                        }
                        break;

                    case Lexer::PunctComma:
                        break;

                    default:
                        std::cerr << "Unexpected punctuation, '" << lexer->getChar() << "' in language-list\n";
                        inList = false;
                        return (SwitchesDeclAST::LANG_DEFAULT);
                }
                break;

            default:
                std::cerr << "Got an unexpected lexical: " << lexer->getType() << "\n";
                inList = false;
                return (SwitchesDeclAST::LANG_DEFAULT);
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            inList = false;
            return (SwitchesDeclAST::LANG_DEFAULT);
        }
    }
    return (retVal);
}

/*
 * This function is responsible for parsing the MAIN switch.  The MAIN
 * switch consists of 3 items.  A MAIN switch contains:
 *
 *  MAIN
 *  =
 *  routine-name
 *
 *  MAIN = routine-name
 *
 * @return the routine-name as a string.
 */
std::string
Switches::handleMain()
{
    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                if (lexer->getKeyword() != KWD::MAIN)
                {
                    std::cerr << "Unrecognized keyword, '" << lexer->getString() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTOperator:
                if (lexer->getOper() != Lexer::OperEqual)
                {
                    std::cerr << "Unrecognized operator in MAIN, '" <<
                            lexer->getChar() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTExplicitDeclared:
                return (lexer->getString());
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got and unexpected lexical: " << lexer->getType() << "\n";
                return ("");
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return ("");
        }
    }
    return ("");
}

/*
 * This function is responsible for parsing the OPTLEVEL switch.
 * The OPTLEVEL switch consists of 3 items.  An OPTLEVEL switch
 * contains:
 *
 *  OPTLEVEL
 *  =
 *  0 | 1 | 2 | 3
 *
 *  OPTLEVEL = 0 | 1 | 2 | 3
 *
 * @return the optimization level as an integer.
 */
uint32_t
Switches::handleOptLevel()
{
    uint32_t retVal = 0;

    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                if (lexer->getKeyword() != KWD::OPTLEVEL)
                {
                    std::cerr << "Unrecognized keyword, '" << lexer->getString() << "'\n";
                    return (0);
                }
                break;

            case Lexer::LTOperator:
                if (lexer->getOper() != Lexer::OperEqual)
                {
                    std::cerr << "Unrecognized operator in OPTLEVEL, '" <<
                            lexer->getChar() << "'\n";
                    return (0);
                }
                break;

            case Lexer::LTDecimalLiteral:
                retVal = lexer->getValue();
                if (retVal > 4)
                {
                    std::cerr << "OPTLEVEL of " << retVal << " is too large.  It must be " <<
                            "0, 1, 2, or 3, setting to 3.\n";
                    retVal = 3;
                }
                return (retVal);
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got and unexpected lexical: " << lexer->getType() << "\n";
                return (0);
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return (0);
        }
    }
    return (0);
}

/*
 * This function is responsible for parsing the VERSION switch.  The
 * VERSION switch consists of 3 items.  A VERSION switch contains:
 *
 *  VERSION
 *  =
 *  quoted-string
 *
 *  VERSION = quoted-string
 *
 *  @return the quoted string as just a string.
 */
std::string
Switches::handleVersion()
{
    while (true)
    {
        switch (lexer->getType())
        {
            case Lexer::LTKeyword:
                if (lexer->getKeyword() != KWD::VERSION)
                {
                    std::cerr << "Unrecognized keyword, '" << lexer->getString() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTOperator:
                if (lexer->getOper() != Lexer::OperEqual)
                {
                    std::cerr << "Unrecognized operator in VERSION, '" <<
                            lexer->getChar() << "'\n";
                    return ("");
                }
                break;

            case Lexer::LTQuotedString:
                return (lexer->getString());
                break;

            case Lexer::LTEmbeddedComment:
            case Lexer::LTTrailingComment:
                break;

            default:
                std::cerr << "Got and unexpected lexical: " << lexer->getType() << "\n";
                return ("");
        }
        if (lexer->getNext() == false)
        {
            std::cerr << "Unexpected end-of-file detected\n";
            return ("");
        }
    }
    return ("");
}
