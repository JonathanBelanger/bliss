/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AST/AttributeAST.h"

using namespace bliss;


/**
 * This function is called to set the structure name associated
 * with this structure-attribute.
 *
 * @param: name - a string containing the name of the structure.
 */
void StructureAttrAST::setName(std::string Name)
{
    if (name.length() == 0)
    {
        name = Name;
    }
    else
    {
        std::cerr << "strcuture-attribute name already set.  New name was, " <<
                     Name << ", and will be ignored.\n";
    }
    return;
}

/**
 * This function is called to set the allocation-actual for
 * this attribute.
 *
 * @param: allocAct - a pointer to AllocationActualAST.
 */
void StructureAttrAST::setAllocActual(AllocationActualAST *allocAct)
{
    if (allocActual == nullptr)
    {
        allocActual = allocAct;
    }
    else
    {
        std::cerr << "structure-attribute allocation-actual already specified.  " <<
                     "Setting again will be ignored\n";
    }
    return;
}
