/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AST/SpecialAST.h"

using namespace bliss;

/**
 * Set the on/off switch, based on the supplied parameter.
 *
 * param: keyword (NO* = off, otherwise on).
 */
void SwitchesDeclAST::setOnOff(KWD::Keyword keyword)
{
    switch (keyword)
    {
        case KWD::CODE:
            onOff.code = true;
            break;

        case KWD::DEBUG:
            onOff.debug = true;
            break;

        case KWD::ERRS:
            onOff.errs = true;
            break;

        case KWD::OPTIMIZE:
            onOff.optimize = true;
            break;

        case KWD::SAFE:
            onOff.safe = true;
            break;

        case KWD::UNAMES:
            onOff.unames = true;
            break;

        case KWD::ZIP:
            onOff.zip = true;
            break;

        case KWD::NOCODE:
            onOff.code = false;
            break;

        case KWD::NODEBUG:
            onOff.debug = false;
            break;

        case KWD::NOERRS:
            onOff.errs = false;
            break;

        case KWD::NOOPTIMIZE:
            onOff.optimize = false;
            break;

        case KWD::NOSAFE:
            onOff.safe = false;
            break;

        case KWD::NOUNAMES:
            onOff.unames = false;
            break;

        case KWD::NOZIP:
            onOff.zip = false;
            break;

        default:
            std::cerr << "Unrecognized on/off switch keyword\n";
            break;
    }
    return;
}

/**
 * Set the language-list, based on the supplied parameter.
 *
 * param: keyword (language).
 */
void SwitchesDeclAST::setLanguage(Lang language)
{

    /*
     * The default language is 'COMMON'.  If we are setting the language for
     * the first time, pop the default off the list.  We may reset it later,
     * but this will prevent a duplicate language being specified.
     *
     * TODO: We need to decide if specifying a language multiple times is
     * really worth reporting.  Maybe we should just swallow it.
     */
    if (langListSet == false)
    {
        languageList.pop_back();
        langListSet = true;
    }
    for (auto l : languageList)
    {
        if (l == language)
        {
            std::cerr << "Language " << language << " already set.\n";
            return;
        }
    }
    if ((language == LANG_COMMON) ||
        (language == LANG_BLISS32) ||
        (language == LANG_BLISS64))
    {
        languageList.push_back(language);
    }
    return;
}

/**
 * Set the list-options, based on the supplied parameter.
 *
 * param: keyword (NO* = off, otherwise on).
 */
void SwitchesDeclAST::setListOptions(KWD::Keyword keyword)
{
    switch (keyword)
    {
        case KWD::SOURCE:
            list.source = true;
            break;

        case KWD::REQUIRE:
            list.require = true;
            break;

        case KWD::EXPAND:
            list.expand = true;
            break;

        case KWD::TRACE:
            list.trace = true;
            break;

        case KWD::LIBRARY:
            list.library = true;
            break;

        case KWD::OBJECT:
            list.object = true;
            break;

        case KWD::ASSEMBLY:
            list.assembly = true;
            break;

        case KWD::SYMBOLIC:
            list.symbolic = true;
            break;

        case KWD::BINARY:
            list.binary = true;
            break;

        case KWD::COMMENTARY:
            list.commentary = true;
            break;

        case KWD::NOSOURCE:
            list.source = false;
            break;

        case KWD::NOREQUIRE:
            list.require = false;
            break;

        case KWD::NOEXPAND:
            list.expand = false;
            break;

        case KWD::NOTRACE:
            list.trace = false;
            break;

        case KWD::NOLIBRARY:
            list.library = false;
            break;

        case KWD::NOOBJECT:
            list.object = false;
            break;

        case KWD::NOASSEMBLY:
            list.assembly = false;
            break;

        case KWD::NOSYMBOLIC:
            list.symbolic = false;
            break;

        case KWD::NOBINARY:
            list.binary = false;
            break;

        case KWD::NOCOMMENTARY:
            list.commentary = false;
            break;

        default:
            std::cerr << "Unrecognized list-option keyword\n";
            break;
    }
    return;
}

/**
 * Set the string switches, based on the supplied parameters.
 *
 * param: keyword
 * param: valueStr
 */
void SwitchesDeclAST::setString(KWD::Keyword keyword, std::string valueStr)
{
    switch (keyword)
    {
        case KWD::IDENT:
            if (ident.length() == 0)
            {
                ident = valueStr;
            }
            else
            {
                std::cerr << "IDENT keyword already set\n";
            }
            break;

        case KWD::LINKAGE:
            if (linkageSet == false)
            {
                linkage = valueStr;
                linkageSet = true;
            }
            else
            {
                std::cerr << "LINKAGE keyword already set\n";
            }
            break;

        case KWD::MAIN:
            if (main.length() == 0)
            {
                main = valueStr;
            }
            else
            {
                std::cerr << "MAIN keyword already set\n";
            }
            break;

        case KWD::VERSION:
            if (version.length() == 0)
            {
                version = valueStr;
            }
            else
            {
                std::cerr << "VERSION keyword already set\n";
            }
            break;

        default:
            std::cerr << "Unrecognized string switch keyword\n";
            break;
    }
    return;
}
