/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Basic/Error.h"

using namespace bliss;

/**
 * This function is called to get a formatted message to display into the listing output.
 *
 * @param message   - This is a formatted string.  If the column start and end values were given, then there will be
 *                    second line containing a string that indicates the start and end locations where the error was
 *                    detected.
 * @param leadingSpaces - This is an integer used to indicate the number of spaces that should be added to the
 *                        beginning of the error position indicator.
 * @return - a value indicating the number of lines returned in the number of output lines in the returned message
 *           parameter.
 */
uint32_t
Error::getString(std::string &message, uint32_t leadingSpaces)
{
    const char msg_first = '%';
    const char msg_rest = '-';
    const std::string msg_prefix = "BLISS-";
    const std::string msg_postfix = "-TEXT, ";
    std::vector<char> sev_code = {'W', 'S', 'E', 'I', 'F'};
    uint32_t cur_line_len = 0;
    uint32_t start_loc = 0;
    uint32_t retVal = 0;

    /*
     * The start of any message line has the following format:
     *
     *  [%-]BLISS-[WSEIF]-TEXT,[space]
     *
     * where the first line starts with '%' and all subsequent lines start with '-'.
     */
    message = msg_first;
    message += msg_prefix;
    message += sev_code[severity];
    message += msg_postfix;
    cur_line_len = message.length();

    /*
     * We only have 130 characters total, per line.  Therefore, if the error message text plus the message header is
     * greater than 130, then we need to break the message into multiple lines.  Either way, we first need to generate
     * a line with as much of the message as possible (possibly all of it).
     *
     * TODO: Currently this code just breaks the line up a the 130th character to be output.  It should actually go to
     *       the space character prior and swallow the space character.
     */
    while (start_loc > text.length())
    {
        uint32_t lenToCopy = std::min(static_cast<unsigned long> (text.length() - start_loc),
                                      static_cast<unsigned long> (130 - cur_line_len));

        /*
         * The above calculation is the minimum of the remaining length of the message text, which is the total length
         * minus the starting location, and the remaining length for an output line, which is 130 minus the current
         * length of the line.
         */
        message += text.substr(start_loc, lenToCopy);
        start_loc += lenToCopy;

        /*
         * If there is more message length to copy, then we need to start a new line, with the message header for a
         * subsequent message line.
         */
        if (start_loc < text.length())
        {
            uint32_t old_cur_line_len = cur_line_len;

            message += '\n';
            message += msg_rest;
            message += sev_code[severity];
            message += msg_postfix;

            /*
             * The next current line length is the new total length of the line minus the old total length of the line
             * minus 1 (for the new-line).
             */
            cur_line_len = message.length() - old_cur_line_len - 1;
        }
        retVal++;    // Increment the number of lines we are generating.
    }

    /*
     * OK, if we have a starting position, then we have either just a starting position or both a starting and ending
     * position.
     */
    if (column_start > 0)
    {
        uint32_t ii;
        message += '\n';
        for (ii = 0; ii < (column_start + leadingSpaces); ii++)
        {
            message += ' ';
        }
        message += '^';
        if (column_end > 0)
        {
            for (ii = 0; ii < (column_end - column_start); ii++)
            {
                message += '-';
            }
            message += '^';
        }
        retVal++;
    }

    /*
     * Return the number of lines generated back to the caller.
     */
    return(retVal);
}

