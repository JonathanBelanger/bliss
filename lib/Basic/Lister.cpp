/*
 * Copyright (C) 2019 Jonathan D. Belanger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Basic/Lister.h"

using namespace bliss;

Lister *Lister::lister = nullptr;

/**
 * Open a listing file.  This must be done before any input files are read, if we are generating a listing.  There is
 * only 1 listing file for each input file specified on the command line.  REQUIRE files may be output in a listing
 * file, but in the listed file for the top most parent input file containing the REQUIRE statement.
 *
 * @param listingFile - a string specifying the listing file to be written.
 * @return true - the listing file has been successfully opened for write.
 *         false - a failure occurred when attempting to open the listing file for write.
 */
bool Lister::open(std::string listingFile)
{

    /*
     * For now, we are just going to use std::cout.
     */
    listingStream = &std::cout;
    return(true);
}

/**
 * Close an opened listing file.
 */
void Lister::close()
{
    listingStream = &std::cout;
    depth = 0;
    currentLine = currentColumn = 1;
    listingLine = "";
    newLineTrigger = '\0';
    pageLine = 0;

    /*
     * For now, we are using std::cout.  No need to close it.
     */
    return;
}

/**
 * This function is called to add a new character to the end of the current listingLine.
 *
 * @param c - An ASCII character value to be added to the listing file.
 */
void Lister::addChar(char c)
{

    /*
     * If we have a CR or LF, then we need to see if we just processed the other.  If so, then we reset the previously
     * processed character and return back to the caller.  Otherwise, we save the character we are processing and
     * convert it to a New-Line (NL).  This will cause the current line to be output.
     *
     * If we do not have a CR or LF, then reset the previously processed character, so that we do not get a false
     * positive later.
     */
    switch (c)
    {
        case 0x0a:  // Line-Feed (LF)
        case 0x0d:  // Carriage-Return (CR)
            if (newLineTrigger == c)
            {
                newLineTrigger = c;
                c = '\n';
            }
            else if (newLineTrigger != '\0')
            {
                newLineTrigger = '\0';
                return;
            }
            break;

        case 0x0c:  // Form-Feed (FF)
            break;

        default:
            newLineTrigger = '\0';
            break;
    }

    /*
     * OK, we know we have something to do.  First, add the character to the listing line.
     */
    currentColumn++;
    listingLine += c;

    /*
     * If the character just added was a New-Line (NL) character, dump out the current listing line and reset it back
     * to a null string.
     */
    if (c == '\n')
    {
        printf("%5u\t%7u %s", depth, currentLine, listingLine.c_str());
        pageLine++;
        if (messages.size() > 0)
        {
            for (auto &message : messages)
            {
                std::string output;

                pageLine += message->getString(output, (8+7+1));
                printf("%s\n", output.c_str());
            }
            messages.clear();
        }
        listingLine = "";
        currentLine++;
        currentColumn = 0;
        updateDepth();
    }
}

/**
 * This function is called to add a message to be output with the next line. The message vector will be reset after
 * each source line is written out.  If start is specified as 0, then no column indicator will be displayed and end
 * is ignored.  If start is specified >0, then if the column indicator is 0, then only the start column will be
 * indicated.  Otherwise, both will be indicated.
 *
 * @param msg - A message string.
 * @param sev - A Severity Level for the message.
 * @param start - A value indicating the starting column of code that generated the message.
 * @param end - A value indicating the ending column of code that generated the message.
 */
void
Lister::addMessage(std::string msg, Error::SeverityLevel sev, uint32_t start, uint32_t end)
{
    Error *err = new Error(msg, sev, start, end);

    messages.push_back(err);
    return;
}
